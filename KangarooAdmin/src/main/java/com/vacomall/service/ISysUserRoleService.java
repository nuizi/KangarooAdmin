package com.vacomall.service;

import com.baomidou.framework.service.ISuperService;
import com.vacomall.entity.SysUserRole;

/**
 *
 * SysUserRole 表数据服务层接口
 *
 */
public interface ISysUserRoleService extends ISuperService<SysUserRole> {
}