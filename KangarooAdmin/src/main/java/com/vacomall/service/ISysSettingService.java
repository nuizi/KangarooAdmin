package com.vacomall.service;

import com.vacomall.entity.SysSetting;
import com.baomidou.framework.service.ISuperService;

/**
 *
 * SysSetting 表数据服务层接口
 *
 */
public interface ISysSettingService extends ISuperService<SysSetting> {


}