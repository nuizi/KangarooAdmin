package com.vacomall.service.impl;

import org.springframework.stereotype.Service;

import com.vacomall.entity.SysUserRole;
import com.vacomall.mapper.SysUserRoleMapper;
import com.vacomall.service.ISysUserRoleService;
import com.vacomall.service.support.BaseServiceImpl;

/**
 *
 * SysUserRole 表数据服务层接口实现类
 *
 */
@Service
public class SysUserRoleServiceImpl extends BaseServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {
}