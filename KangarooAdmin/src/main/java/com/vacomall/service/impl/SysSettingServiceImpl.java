package com.vacomall.service.impl;

import org.springframework.stereotype.Service;

import com.vacomall.mapper.SysSettingMapper;
import com.vacomall.entity.SysSetting;
import com.vacomall.service.ISysSettingService;
import com.vacomall.service.support.BaseServiceImpl;

/**
 *
 * SysSetting 表数据服务层接口实现类
 *
 */
@Service
public class SysSettingServiceImpl extends BaseServiceImpl<SysSettingMapper, SysSetting> implements ISysSettingService {


}