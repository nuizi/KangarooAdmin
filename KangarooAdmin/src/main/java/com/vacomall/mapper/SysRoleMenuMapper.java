package com.vacomall.mapper;

import com.vacomall.entity.SysRoleMenu;
import com.baomidou.mybatisplus.mapper.AutoMapper;

/**
 *
 * SysRoleMenu 表数据库控制层接口
 *
 */
public interface SysRoleMenuMapper extends AutoMapper<SysRoleMenu> {


}